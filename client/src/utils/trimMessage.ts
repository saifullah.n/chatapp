export function trimMessage(message: String, count: number) {
  let result = message;
  if (message.length > count) {
    result = message.slice(0, count) + '...';
  }
  return result;
}
