import axios from 'axios';
axios.defaults.baseURL = 'http://localhost:3000';
axios.interceptors.request.use(function (config) {
  const accessToken = sessionStorage.getItem('token');
  if (config.url !== 'login' && config.url !== 'register') {
    config.headers['Authorization'] = `Bearer ${accessToken || ''}`;
  }
  return config;
});

export default axios;
