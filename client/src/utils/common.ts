export const isCommonAvailabe = (setA = new Set(), setB = new Set()) => {
  for (const element of new Set(setB)) {
    if (setA.has(element)) {
      return true;
    }
  }
  return false;
};
export const getChatDisplayName = (chatName: String, myName: String) => {
  const userNames = chatName.split('+');
  const otherUser = userNames.filter((i) => i != myName);
  return otherUser[0];
};
export const formatDateTime = (date_time: Date) => {
  const date = new Date(date_time);
  return date.toLocaleTimeString(navigator.language, {
    hour: '2-digit',
    minute: '2-digit',
  });
};
