export type UserInfoResponse = {
  name: String;
  user_id: String;
};
