export type Message = {
  _id: string;
  chatId: string;
  senderId: { _id: string; name: string };
  content: string;
  readBy: { userId: string; readTime: Date }[];
  createdAt: Date;
  updatedAt?: Date;
};
