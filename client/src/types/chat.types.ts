export type Chat = {
  _id: string;
  chatName: string;
  members: { _id: string; name: string }[];
  isGroupChat: Boolean;
  lastMessage: String;
  latestMessage: String;
};
