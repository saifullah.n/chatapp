import React from "react";
import { FiSearch } from 'react-icons/fi';
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import { createAvatar } from '@dicebear/core';
import { lorelei } from '@dicebear/collection';
import search from '../assets/images/Search-rafiki.png';
import { UserInfoResponse } from "../types/user.types";

type UserSearchResults = {
    _id: string;
    name: string;
    email: string;
};

interface Props {
    clickEventHandler: (user: UserInfoResponse) => void;
    group: Boolean
}

export function SearchUser({
    clickEventHandler,
    group
}: Props){
    const [searchTerm, setSearchTerm] = useState('');
    const [searchResults, setSearchResults] = useState<UserSearchResults[]>([]);
    useEffect(() => {
        if (searchTerm.trim() === '') {
          setSearchResults([]);
          return;
        }
        axios
          .get(
            '/user/searchForUser?' +
              new URLSearchParams({
                term: searchTerm,
              })
          )
          .then((response) => {
            if (response.status === 200 || response.status === 201) {
              setSearchResults(response.data.usersList);
            } else {
              console.log(response);
            }
          })
          .catch((err) => console.log(err));
      }, [searchTerm]);
    return(
    <React.Fragment>
        <div className="search-form-input">
            <input
                name="term"
                id="term"
                type="text"
                value={searchTerm}
                onChange={(ev) => {
                setSearchTerm(ev.target.value);
                }}
            />
            <FiSearch className="form-icons" />
            </div>
            <div className="search-results">
            {searchResults.length ? (
                searchResults.map((e, i) => {
                const avatar = createAvatar(lorelei, {
                    seed: e?.name,
                }).toDataUriSync();
                return (
                    <div
                    key={i}
                    className="search-result"
                    onClick={() => {
                        setSearchTerm("");
                        clickEventHandler({user_id: e._id, name:e.name});
                    }}
                    >
                    <img src={avatar} alt="" className={'profilePic img'} />
                    <span>
                        {e.name}
                        <p className="small-text">{e.email}</p>
                    </span>
                    </div>
                );
                })
            ) : (!group &&
                <img src={search} alt="" className="search-illustration" />
            )}
        </div>
    </React.Fragment>
    )
}