import React from 'react';
import { IoMdSettings } from 'react-icons/io';
import { createAvatar } from '@dicebear/core';
import { lorelei } from '@dicebear/collection';

export function SideBar({ username }: { username: string }) {
  return (
    <React.Fragment>
      <div className="flex-down">
        <p className="logo-header small">
          Chit Chat<span>.</span>
        </p>
        <div>
          <img
            src={createAvatar(lorelei, {
              seed: username,
            }).toDataUriSync()}
            alt=""
            className="userLogo img"
          />
          <br></br>
          <br></br>
          <IoMdSettings className="icons settings" />
        </div>
      </div>
    </React.Fragment>
  );
}
