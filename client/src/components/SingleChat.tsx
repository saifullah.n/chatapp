import React, { useEffect, useRef } from 'react';
import { createAvatar } from '@dicebear/core';
import { lorelei } from '@dicebear/collection';
import { AiOutlinePlus } from 'react-icons/ai';
import { BiSolidSend } from 'react-icons/bi';
import leftArrow from '../assets/images/left-arrow.png';
import { formatDateTime } from '../utils/common';
import { Tooltip, Typography } from '@mui/material';
import { Message } from '../types/message.types';

interface Props {
  messages: Message[];
  online: Boolean;
  chatName: string;
  chatMembers: { _id: string; name: string }[];
  userId: String;
  sendMessageHandler: (msg: String) => void;
  closeChatHandler: () => void;
  msgContent: string;
  setMsgContent: (msg: string) => void;
  typingUser: String;
}

export function SingleChat({
  messages,
  online,
  chatName,
  userId,
  chatMembers,
  sendMessageHandler,
  closeChatHandler,
  msgContent,
  setMsgContent,
  typingUser
}: Props) {
  const messagesEndRef = useRef<null | HTMLDivElement>(null);
  const avatar = createAvatar(lorelei, {
    seed: chatName,
  }).toDataUriSync();
  const chatMembersMap: {
    [key: string]: string;
  } = {};
  chatMembers.forEach((i) => {
    chatMembersMap[i._id] = i.name;
  });
  useEffect(()=>{
    messagesEndRef.current?.scrollIntoView({ 
      block: "nearest",
      inline: "center",
      behavior: "smooth",
    });
  }, [messages])
  return (
    <React.Fragment>
      <div className="messages">
        <div>
          <div className="profile-header">
            <img
              src={leftArrow}
              alt=""
              className="icon-img"
              onClick={closeChatHandler}
            />
            <img
              src={avatar}
              alt=""
              className={online ? 'online img' : 'profilePic img'}
            />
            <div className="ml-15">
              <p className="profile-name m-0">{chatName}</p>
              <p className="secondary-color mt-5">
                {online ? 'Online' : 'Offline'}
              </p>
            </div>
          </div>

          <div className="message-stacks">
            {[...messages].map((msg, i) => {
              console.log({msg});
              const senderId = msg.senderId._id ?? msg.senderId;
              const readByUsers = [...msg.readBy].map((readReceipt) => {
                const readerName = chatMembersMap[readReceipt.userId];
                const readTime = formatDateTime(readReceipt.readTime);
                return readerName + ' at ' + readTime;
              });
              return (
                <div key={i}>
                  <div
                    key={i}
                    className={senderId === userId ? 'me-stack' : 'you-stack'}
                  >
                    <div>
                      {senderId === userId ? (
                        <Tooltip
                          title={
                            <Typography>
                              Read By: <br />
                              {readByUsers.map((i, k) => (
                                <React.Fragment key={k}>
                                  {i}
                                  <br />
                                </React.Fragment>
                              ))}
                            </Typography>
                          }
                          placement="top"
                          arrow
                        >
                          <p>{msg.content}</p>
                        </Tooltip>
                      ) : (
                        <p>{msg.content}</p>
                      )}

                      {senderId === userId ? (
                        <p className="text-right m-0">
                          <span className="small-text">{formatDateTime(msg.createdAt)} </span>
                           You
                        </p>
                      ) : (
                        <p className="m-0">
                          <span className="small-text">{formatDateTime(msg.createdAt)} </span>
                           {msg.senderId.name}
                        </p>
                      )}
                    </div>
                  </div>
                </div>
              );
            })}
            <div ref={messagesEndRef} className='scrollable'></div>
          </div>
          { typingUser && <p className="you-stack">{typingUser} is typing...</p>}
          <div className="send-message">
            <div className="send-form-input">
              <AiOutlinePlus className="add-icon" />
              <input
                name="term"
                id="term"
                type="text"
                placeholder="Type Something..."
                value={msgContent}
                onChange={(ev) => {
                  setMsgContent(ev.target.value);
                }}
              />
              <BiSolidSend
                className="send-icon"
                onClick={() => {
                  sendMessageHandler(msgContent);
                  setMsgContent('');
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
