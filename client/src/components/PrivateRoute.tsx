import { ReactNode } from 'react';
import { Navigate } from 'react-router-dom';

const PrivateRoute = ({ children }: { children: ReactNode }) => {
  const accessToken = sessionStorage.getItem('token');
  return accessToken ? children : <Navigate to="/login" />;
};

export default PrivateRoute;
