/* eslint-disable react/prop-types */
import React from 'react';
import { trimMessage } from '../utils/trimMessage';

interface Props {
  chatClassName: string;
  profile: string;
  online: Boolean;
  name: String;
  message: String;
  unread: Boolean;
}

export function Tile(props: Props) {
  return (
    <React.Fragment>
      <div className={props.chatClassName}>
        <div className="flex j-between">
          <img
            src={props.profile}
            alt=""
            className={props.online ? 'online img' : 'profilePic img'}
          />
          <div className="content">
            <p className="name">{props.name}</p>
            <p className="msg">{trimMessage(props.message, 30)}</p>
          </div>
          {props.unread && (
            <div className="active">
              <span></span>
            </div>
          )}
        </div>
      </div>
    </React.Fragment>
  );
}
