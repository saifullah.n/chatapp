import React, { useEffect, useState } from 'react';
import { BiSolidSearch } from 'react-icons/bi';
import downArrow from '../assets/images/down-arrow.png';
import { Tile } from './Tile';
import { createAvatar } from '@dicebear/core';
import { lorelei } from '@dicebear/collection';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import { CgNametag } from 'react-icons/cg';
import axios from 'axios';
import { isCommonAvailabe, getChatDisplayName } from '../utils/common';
import { toast } from 'react-toastify';
import { Chat } from '../types/chat.types';
import { SearchUser } from './SearchUser';
import { UserInfoResponse } from '../types/user.types';
import { MdCancel } from "react-icons/md";
import * as ChatTypes from '../types/chat.types';

interface Props {
  chats: Chat[];
  username: String;
  onlineUsers: Set<String>;
  activeChat: Chat | null;
  setActiveChat: (chat: Chat) => void;
  setChats: (chats: ChatTypes.Chat[]) => void;
  user: UserInfoResponse;
}
export function Chats({
  chats,
  username,
  onlineUsers,
  activeChat,
  setActiveChat,
  setChats,
  user
}: Props) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [openGroup, setOpenGroup] = React.useState(false);
  const handleOpenGroup = () => setOpenGroup(true);
  const handleCloseGroup = () => setOpenGroup(false);
  const [groupUsersList, setGroupUsersList] = useState<UserInfoResponse[]>([]);
  const [chatName, setChatName] = useState<String>("");
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    height: 400,
    bgcolor: '#343642',
    overflowY: 'auto',
    overflowX: 'hidden',
    p: 3,
    borderRadius: 2,
  };
  
  console.log({groupUsersList}, groupUsersList.length);

  function updateUsersList(userObj: UserInfoResponse){
    if(groupUsersList.some((element) => element.user_id === userObj.user_id)){
      setGroupUsersList(groupUsersList.filter(user => user.user_id!==userObj.user_id));
    }
    else{
      setGroupUsersList([...groupUsersList, userObj]);
    }
  }

  function createChat(user: UserInfoResponse) {
    axios
      .post('/chat/createChat', {
        userId: user.user_id,
      })
      .then((response) => {
        if (response.status === 200 || response.status === 201) {
          setChats([...chats, response.data.chat.chatMsg]);
          setActiveChat(response.data.chat.chatMsg);
          handleClose();
        } else {
          toast.error('Something went Wrong!', {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      })
      .catch((err) => console.log(err));
  }

  function createGroupChat(){
    const membersList = [user.user_id];
    for(let user of groupUsersList){
      membersList.push(user.user_id);
    }
    axios
    .post('/chat/createGroupChat', {
      chatName,
      members:membersList,
      isGroupChat: true
    })
    .then((response) => {
      if (response.status === 200 || response.status === 201) {
        setChats([...chats, response.data.chat]);
        setActiveChat(response.data.chat);
        handleClose();
      } else {
        toast.error('Something went Wrong!', {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    })
    .catch((err) => console.log(err));
    handleCloseGroup();
  }

  return (
    <React.Fragment>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <SearchUser clickEventHandler={createChat} group={false}/>
        </Box>
      </Modal>
      <Modal
        open={openGroup}
        onClose={handleCloseGroup}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div className="search-form-input">
            <input
              name="term"
              id="term"
              type="text"
              value={chatName}
              onChange={(ev)=>setChatName(ev.target.value)}
              placeholder="Group Name"
            />
            <CgNametag className="form-icons" />
          </div>
          <div className='selected-users'>
            {groupUsersList ? [...groupUsersList].map((e, i)=>{
              return(
              <div key={i}>
                  <span>{e.name} </span><span className='icons'><MdCancel onClick={()=>{updateUsersList(e)}}/></span>
              </div>
              )
            }):""}
          </div>
          <SearchUser clickEventHandler={updateUsersList} group={true}/>
          <br/>
          <div className='text-center'>
            <button className='btn' onClick={()=>{createGroupChat()}}>Create Chat</button>
          </div>
        </Box>
      </Modal>
      <div
        className={activeChat ? 'chats chat-invisible' : 'chats chat-visible'}
      >
        <div className="flex j-between">
          <h1 className="m-0">Chats</h1>
          <div onClick={handleOpen}>
            <BiSolidSearch className="icons search" />
          </div>
        </div>
        <div className='text-right'>
          <button className='btn btn-small' onClick={handleOpenGroup}>Create Group</button>
        </div>
        <div className="chat-block">
          <div className="flex j-between">
            <p className="secondary-color">Welcome {username}</p>
            <img src={downArrow} alt="" className="icon-img" />
          </div>
          {[...chats].map((e, i) => {
            const avatar = createAvatar(lorelei, {
              seed: e.chatName,
            }).toDataUriSync();
            return (
              <div
                key={e._id}
                onClick={() => {
                  setActiveChat(e);
                }}
              >
                <Tile
                  online={isCommonAvailabe(
                    onlineUsers,
                    new Set([...e.members].map((i) => i._id))
                  )}
                  profile={avatar}
                  name={
                    e.isGroupChat
                      ? e.chatName
                      : getChatDisplayName(e.chatName, username)
                  }
                  message={e.lastMessage}
                  unread={true}
                  chatClassName={
                    activeChat?._id == e._id ? 'chat-active' : 'chat'
                  }
                />
              </div>
            );
          })}
        </div>
      </div>
    </React.Fragment>
  );
}
