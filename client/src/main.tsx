import ReactDOM from 'react-dom/client';
import App from './App';
import './utils/axiosUtils.js';
import './index.css';
import 'react-toastify/dist/ReactToastify.css';
import { GoogleOAuthProvider } from '@react-oauth/google';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <GoogleOAuthProvider clientId="576257602949-g04cq8nj7tpd7ap543aevjbl0qjdhkop.apps.googleusercontent.com">
    <App />
  </GoogleOAuthProvider>
);
