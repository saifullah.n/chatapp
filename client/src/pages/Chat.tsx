import React, { useEffect, useRef, useState } from 'react';
import io from 'socket.io-client';
import { Chats } from '../components/Chats';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { SideBar } from '../components/SideBar';
import { SingleChat } from '../components/SingleChat';
import { getChatDisplayName, isCommonAvailabe } from '../utils/common';
import noMessage from '../assets/images/cute-chicken.png';
import { Message } from '../types/message.types';
import { UserInfoResponse } from '../types/user.types';
import * as ChatTypes from '../types/chat.types';

export function Chat() {
  const SOCKET_IO = io('http://localhost:3000/user-namespace', {
    autoConnect: false,
    auth: {
      token: sessionStorage.getItem('token'),
    },
  });
  const navigate = useNavigate();
  const [user, setUser] = useState<UserInfoResponse>();
  const [msgContent, setMsgContent] = useState<string>('');
  const [socketState, setSocketState] = useState('INITIAL');
  const [chats, updateChat] = useState<ChatTypes.Chat[]>([]);
  const [socket, setSocket] = useState(SOCKET_IO);
  const [onlineUsers, updateOnlineUsers] = useState<Set<String>>(new Set());
  const [activeChat, updateActiveChat] = useState<ChatTypes.Chat | null>(null);
  const [messages, updateMessages] = useState<Message[]>([]);
  const [typingUser, setTypingUser] = useState<String>('');
  const chatRef = useRef(chats);
  const activeChatRef = useRef(activeChat);
  const onlineUsersRef = useRef(onlineUsers);
  const messagesRef = useRef(messages);
  const USERNAME = sessionStorage.getItem('username') || '';
  const setMessages = (data: Message[]) => {
    messagesRef.current = data;
    updateMessages(data);
  };
  const setOnlineUsers = (data: Set<String>) => {
    onlineUsersRef.current = data;
    updateOnlineUsers(data);
  };
  const setChats = (data: ChatTypes.Chat[]) => {
    chatRef.current = [...data];
    updateChat(data);
  };
  const setActiveChat = (data: ChatTypes.Chat | null) => {
    activeChatRef.current = data;
    updateActiveChat(data);
  };
  const onChatClick = (chat: ChatTypes.Chat) => {
    socket.emit('JOIN_CHAT', {
      prevChatId: activeChat ? activeChat._id : null,
      currChatId: chat._id,
    });
    setActiveChat(chat);
    setMessages([]);
  };
  const onSendMessage = (msg: String) => {
    socket.emit('SEND_MESSAGE', {
      senderName: user?.name,
      senderId: user?.user_id,
      chatId: activeChat?._id,
      content: msg,
    });
  };
  const onCloseChat = () => {
    socket.emit('LEAVE_CHAT', activeChat?._id);
    setActiveChat(null);
  };
  useEffect(() => {
    const getUserInfo = async () => {
      try {
        const response = await axios.get('/user/get-info');
        setUser(response.data);
      } catch (error) {
        sessionStorage.clear();
        navigate('/login');
      }
    };
    getUserInfo();
  }, []);
  useEffect(() => {
    if(msgContent){
      socket.emit("ON_TYPE", {
        currChatId : activeChat?._id,
        user : user
      })
    }
    else if(activeChat){
      socket.emit("NO_ACTIVE_INBOX_INPUT", {
        currChatId : activeChat._id 
      })
    }
  }, [msgContent]);
  useEffect(() => {
    if (user && user.name) {
      socket.connect();
      socket.on('connect', () => {
        setSocketState('SUCCEEDED');
      });
      socket.on('GET_CHATS', (docs) => {
        setChats(docs);
      });
      socket.on('GET_ONLINE_USERS', (userIdArr) => {
        let newOnlineUsers = new Set(onlineUsersRef.current);
        for (const userId of userIdArr) {
          if (userId !== user.user_id) newOnlineUsers.add(userId);
        }
        setOnlineUsers(newOnlineUsers);
      });
      socket.on('GET_OFFLINE_USERS', (userIdArr) => {
        let newOnlineUsers = new Set(onlineUsersRef.current);
        for (const userId of userIdArr) newOnlineUsers.delete(userId);
        setOnlineUsers(newOnlineUsers);
      });
      socket.on('GET_TYPED_USER', (user) => {
        setTypingUser(user.name);
      })
      socket.on('NO_TYPING_USER', () => {
        setTypingUser('');
      })
      socket.on('GET_MESSAGES', (msgArr: Message[]) => {
        setMessages([...messagesRef.current, ...msgArr]);
        let unReadMsgIdArr = [];
        for (let i = msgArr.length - 1; i >= 0; i--) {
          if (msgArr[i].senderId._id === user.user_id) continue;
          const currReadUsers = msgArr[i].readBy;
          if (currReadUsers.findIndex((i) => i.userId === user.user_id) >= 0)
            break;
          unReadMsgIdArr.push(msgArr[i]._id);
        }
        socket.emit('UPDATE_READ', {
          chatId: activeChatRef.current?._id,
          userId: user.user_id,
          messageIds: unReadMsgIdArr,
        });
      });
      socket.on('RECENT_MESSAGE', ({ chatId, content }) => {
        let newChats = chatRef.current;
        let updatedChatIdx = newChats.findIndex((chat) => chat._id === chatId);
        // Checking whether the client is subscribed to the chat
        if (updatedChatIdx !== -1) {
          newChats[updatedChatIdx].lastMessage = content;
          setChats(newChats);
        }
      });
      socket.on('READ_RECEIPT', ({ messageIds, dateTime, userId }) => {
        const currMessages = [...messagesRef.current];
        let updatedMessages = currMessages.map((msg) => {
          if ([...messageIds].includes(msg._id)) {
            let oldReadBy = [...msg.readBy];
            oldReadBy.push({ userId: userId, readTime: dateTime });
            msg.readBy = oldReadBy;
          }
          return msg;
        });
        setMessages(updatedMessages);
      });
      socket.on('connect_error', (err) => {
        console.log(err);
        setSocketState('FAILED');
      });
    }
    return () => {
      if (socket.connected) socket.disconnect();
    };
  }, [user]);
  if (socketState === 'FAILED') {
    return <p>Something went wrong {':)'}</p>;
  }
  if (socketState === 'INITIAL' || !user) {
    return <p>Loading...</p>;
  }
  return (
    <React.Fragment>
      <div className="flex j-between wrapper">
        <div className="sidebar">
          <SideBar username={USERNAME} />
        </div>
        <Chats
          chats={chats}
          username={USERNAME}
          onlineUsers={onlineUsers}
          setActiveChat={onChatClick}
          activeChat={activeChat}
          setChats={setChats}
          user={user}
        />
        <div className="singleChat">
          {activeChat ? (
            <SingleChat
              closeChatHandler={onCloseChat}
              sendMessageHandler={onSendMessage}
              chatMembers={activeChat?.members}
              chatName={
                getChatDisplayName(activeChat?.chatName, USERNAME) ?? ''
              }
              msgContent={msgContent}
              typingUser = {typingUser}
              setMsgContent={setMsgContent}
              messages={messages}
              userId={user.user_id}
              online={isCommonAvailabe(
                onlineUsers,
                new Set(activeChat?.members?.map((i) => i?._id))
              )}
            />
          ) : (
            <div className="no-message chat-invisible">
              <img src={noMessage} alt="" />
              <br />
              <p>Select a chat to display messages</p>
              <p className="logo-header">
                Chit Chat<span>.</span>
              </p>
            </div>
          )}
        </div>
      </div>
    </React.Fragment>
  );
}
