import React, { useState, FormEvent } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { HiMiniUserCircle } from 'react-icons/hi2';
import { HiMail } from 'react-icons/hi';
import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';
import chicken from '../assets/images/cute-chicken1.png';
import axios from '../utils/axiosUtils';
import { toast } from 'react-toastify';

export function Register() {
  const [firstName, setfirstName] = useState('');
  const [lastName, setlastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [disabled, setDisable] = useState(false);
  const [inputType, setInputType] = useState("password");
  const navigate = useNavigate();

  async function registerUser(event: FormEvent) {
    event.preventDefault();
    console.log("here");
    setDisable(true);
    axios
      .post('/user/register', {
        name: firstName + lastName,
        email,
        password,
      })
      .then((response) => {
        if (response.status === 200 || response.status === 201) {
          sessionStorage.setItem('token', response.data.accessToken);
          toast.success('Registration successful', {
            position: toast.POSITION.TOP_RIGHT,
          });
          navigate('/');
        } else {
          toast.error('Registration failed', {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      })
      .catch((err) => console.log(err));
    setDisable(false);
  }
  return (
    <React.Fragment>
      <nav>
        <p className="logo-header">
          Chit Chat<span>.</span>
        </p>
      </nav>
      <div className="flex">
        <div className="auth">
          <h1 className="auth-header">
            Create new account<span>.</span>
          </h1>
          <h4 className="secondary-color">
            Already a Member?{' '}
            <Link to={'/login'} className="links">
              Log in
            </Link>
          </h4>
          <form
            method="POST"
            onSubmit={(e) => {
              registerUser(e);
            }}>
            <div className="half-form-input">
              <p className="form-label">First name</p>
              <input
                name="firstName"
                id="firstName"
                type="text"
                value={firstName}
                onChange={(ev) => {
                  setfirstName(ev.target.value);
                }}
              />
              <HiMiniUserCircle className="form-icons" />
            </div>
            <div className="half-form-input">
              <p className="form-label">Last name</p>
              <input
                name="lastName"
                id="lastName"
                type="text"
                value={lastName}
                onChange={(ev) => {
                  setlastName(ev.target.value);
                }}
              />
              <HiMiniUserCircle className="form-icons" />
            </div>
            <div className="form-input">
              <p className="form-label">email</p>
              <input
                name="email"
                id="email"
                type="email"
                value={email}
                onChange={(ev) => {
                  setEmail(ev.target.value);
                }}
              />
              <HiMail className="form-icons" />
            </div>
            <div className="form-input">
              <p className="form-label">Password</p>
              <input
                name="password"
                id="password"
                type={inputType}
                value={password}
                onChange={(ev) => {
                  setPassword(ev.target.value);
                }}
              />
              {inputType==="password" ? <AiFillEye className="form-icons" onClick={()=>{setInputType("text")}} /> : <AiFillEyeInvisible className="form-icons" onClick={()=>{setInputType("password")}} />}
            </div>
            <input
              type="submit"
              className="btn"
              value={'Create Account'}
              disabled={disabled}
            />
          </form>
        </div>
        <img src={chicken} alt="" className="illustration" />
      </div>
    </React.Fragment>
  );
}
