import React, { useState, FormEvent } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { HiMail } from 'react-icons/hi';
import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';
import chicken from '../assets/images/cute-chicken1.png';
import axios from '../utils/axiosUtils';
import { toast } from 'react-toastify';
import { useGoogleOneTapLogin } from '@react-oauth/google';
import { AxiosResponse } from "axios"

export function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [disabled, setDisable] = useState(false);
  const [inputType, setInputType] = useState("password");
  const navigate = useNavigate();

  async function postLogin (response : AxiosResponse) {
    if (response.status === 200 || response.status === 201) {
      sessionStorage.setItem('token', response.data.accessToken);
      sessionStorage.setItem('username', response.data.userData.name);
      toast.success('Logged In', {
        position: toast.POSITION.TOP_RIGHT,
      });
      navigate('/');
    } else {
      toast.error('Authentication failed', {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
  }

  useGoogleOneTapLogin({
    onSuccess: (credentialResponse) => {
      console.log(credentialResponse);
      axios
      .post('/user/login', {
        "GoogleAccessToken" : credentialResponse.credential
      }).then((response) => {
        postLogin(response);
      })
      .catch(() => {
          toast.error('Authentication failed', {
            position: toast.POSITION.TOP_RIGHT,
          })}
      );
    },
    onError: () => {
      toast.error('Authentication failed', {
        position: toast.POSITION.TOP_RIGHT,
      });
    },
  });

  async function loginUser(event: FormEvent) {
    event.preventDefault();
    setDisable(true);
    axios
      .post('/user/login', {
        email,
        password,
      })
      .then((response) => {
        postLogin(response);
      })
      .catch((err) => console.log(err));
    setDisable(false);
  }
  return (
    <React.Fragment>
      <nav>
        <p className="logo-header">
          Chit Chat<span>.</span>
        </p>
      </nav>
      <div className="flex">
        <div className="auth">
          <h1 className="auth-header">
            Log in to your account<span>.</span>
          </h1>
          <h4 className="secondary-color">
            New in here?{' '}
            <Link to={'/register'} className="links">
              Register
            </Link>
          </h4>
          <form
            method="POST"
            onSubmit={(e) => {
              loginUser(e);
            }}
          >
            <div className="form-input">
              <p className="form-label">email</p>
              <input
                name="email"
                id="email"
                type="email"
                value={email}
                onChange={(ev) => {
                  setEmail(ev.target.value);
                }}
              />
              <HiMail className="form-icons" />
            </div>
            <div className="form-input">
              <p className="form-label">Password</p>
              <input
                name="password"
                id="password"
                type={inputType}
                value={password}
                onChange={(ev) => {
                  setPassword(ev.target.value);
                }}
              />
              {inputType==="password" ? <AiFillEye className="form-icons" onClick={()=>{setInputType("text")}} /> : <AiFillEyeInvisible className="form-icons" onClick={()=>{setInputType("password")}} />}
            </div>
           
            <input
              type="submit"
              className="btn"
              value={'Log in'}
              disabled={disabled}
            />
          </form>
        </div>
        <img src={chicken} alt="" className="illustration" />
      </div>
    </React.Fragment>
  );
}
