import "dotenv/config.js";
import jwt from "jsonwebtoken";
import CustomError from "../utils/CustomError.js";
import { Socket } from "socket.io";
import { NextFunction, Request, Response } from "express";

// Verify whether the token is valid
export const verifyToken = (
  socket: Socket,
  next: (err?: CustomError) => void
) => {
  const jwtToken = socket.handshake.auth.token;
  if (!jwtToken) {
    const err = new CustomError("Token not found", 401);
    next(err);
    return;
  }
  let verified;
  try {
    verified = jwt.verify(jwtToken, process.env.ACCESS_SECRET);
    let userDetails = verified as {
      name: string;
      user_id: string;
    };
    socket.userId = userDetails.user_id;
    socket.userName = userDetails.name;
  } catch (error) {
    const err = error as Error;
    if (err.name === "TokenExpiredError")
      next(new CustomError("Session expired", 401));
    else next(new CustomError("Access denied", 401));
  }
  next();
};
export const httpVerifyToken = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.headers.authorization;
  if (!token) {
    const err = new CustomError("Token not found", 401);
    return next(err);
  }
  const bearer = token.split(" ")[0];
  if (!bearer || bearer.toString().toLowerCase() !== "bearer") {
    const err = new CustomError("Bearer token needed", 401);
    return next(err);
  }
  const jwtToken = token.split(" ")[1];
  let verified;
  try {
    verified = jwt.verify(jwtToken, process.env.ACCESS_SECRET);
  } catch (error) {
    const err = error as Error;
    if (err.name === "TokenExpiredError")
      throw new CustomError("Session expired", 401);
    else throw new CustomError("Access denied", 401);
  }
  let userDetails = verified as {
    name: string;
    user_id: string;
  };
  req.user_id = userDetails.user_id;
  req.name = userDetails.name;
  next();
};
