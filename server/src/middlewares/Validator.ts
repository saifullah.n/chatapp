import Joi from "joi";
import asyncErrorHanlder from "../utils/asyncErrorHandler.js";

type ValidatorsType = {
  [key: string]: Joi.ObjectSchema<any>;
};

export default (Validators: ValidatorsType) => {
  return (validator: string, property: "body" | "query" = "body") => {
    return asyncErrorHanlder(async (req, res, next) => {
      const validated = await Validators[validator].validateAsync(
        req[property]
      );
      req.body = validated;
      next();
    });
  };
};
