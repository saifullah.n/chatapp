import express from "express";
import CustomError from "./utils/CustomError.js";
import UserRoutes from "./features/User/user.route.js";
import ChatRoutes from "./features/Chat/chat.route.js";
import "dotenv/config.js";

const router = express.Router();

// Feature Routes
router.use("/user", UserRoutes);
router.use("/chat", ChatRoutes);

// Welcome API
router.get("/", (req, res, next) => {
  res.send("Hello from Backend");
});

// Handling 404 routes
router.all("*", (req, res, next) => {
  const err = new CustomError(
    `Can't find ${req.originalUrl} on the server!`,
    404
  );
  next(err);
});

export default router;
