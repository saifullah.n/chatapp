import CustomError from "./CustomError.js";

type DbCallFunction = (...args: any[]) => Promise<any>;
type WrappedDbCallFunction = DbCallFunction;
type MongoError = {
  name: string;
  code: number;
  message: string;
  keyValue: { [key: string]: string };
};

export default (dbCall: DbCallFunction): WrappedDbCallFunction => {
  return async (...arg) => {
    try {
      return await dbCall(...arg);
    } catch (error) {
      let err = error as MongoError;
      if (err.name === "ValidationError") {
        throw new CustomError(err.message);
      }
      if (err.name === "MongoServerError") {
        if (err.code === 11000)
          throw new CustomError(
            JSON.stringify(err.keyValue) + " Already found"
          );
        throw new CustomError("Error in reaching DB");
      }
      throw new CustomError("Error in reaching DB");
    }
  };
};
