export {};

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV: "development" | "production";
      DB_CONNECTION_URL: string;
      ACCESS_SECRET: string;
    }
  }
}
