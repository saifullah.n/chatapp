import express from "express";
import { createServer } from "http";
import { Server } from "socket.io";
import cors from "cors";
import index from "./index.js";
import GlobalErrorHandler from "./utils/asyncErrorHandler.js";
import { mongoConnect } from "./features/Mongoose/connect.service.js";
import "dotenv/config.js";
import userSocket from "./namespaces/user.namespace.js";

const app = express();

const corsOption = {
  origin: true,
  credentials: true
};
app.use(cors(corsOption));

// Parsing Request
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//REST endpoints
app.use(index);

// Socket setup
const server = createServer(app);

// WebSocker initialisation
const io = new Server(server, {
  cors: {
    origin: "*"
  }
});
const userNamespace = io.of("/user-namespace");
userSocket(userNamespace);

//Global Error Handler
app.use(GlobalErrorHandler);

// Start server once mongo est. conn
await mongoConnect(() => {
  server.listen(3000, () => {
    console.log("Server started at PORT 3000");
  });
});
