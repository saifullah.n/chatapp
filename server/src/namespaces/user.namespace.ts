import UserService from "../features/User/user.service.js";
import * as ChatService from "../features/Chat/chat.service.js";
import { verifyToken } from "../middlewares/verifyToken.js";
import { MessageService } from "../features/Message/message.service.js";
import { Socket, Namespace } from "socket.io";
const userSocket = (userNamespace: Namespace) => {
  userNamespace.use(verifyToken);
  // Establishing connection
  userNamespace.on("connection", async (socket: Socket) => {
    let userId = socket.userId; // Setting userId at verifyToken

    const onlineUsers = await UserService.getOnlineUser(userId);
    const onlineUserIds = [...onlineUsers].map((i) => i._id.toString());
    socket.emit("GET_ONLINE_USERS", onlineUserIds);

    await UserService.updateOnlineStatus(userId, true);
    socket.broadcast.emit("GET_ONLINE_USERS", [userId]);

    const userChats = await ChatService.getChatsForUser(userId);
    socket.emit("GET_CHATS", userChats);

    socket.on("JOIN_CHAT", async ({ prevChatId, currChatId }) => {
      if (prevChatId !== null) socket.leave(prevChatId);
      socket.join(currChatId);
      const messages = await MessageService.findMessagesByChatId(currChatId);
      socket.emit("GET_MESSAGES", messages);
    });
    socket.on("LEAVE_CHAT", async (currChatId) => {
      socket.leave(currChatId);
    });
    socket.on("ON_TYPE", async ({currChatId, user}) => {
      socket.to(currChatId).emit("GET_TYPED_USER", user);
    });
    socket.on("NO_ACTIVE_INBOX_INPUT", async({currChatId}) => {
      socket.to(currChatId).emit("NO_TYPING_USER")
    })
    socket.on(
      "SEND_MESSAGE",
      async ({ senderName, senderId, content, chatId }) => {
        const message = await MessageService.createNewMessage(
          chatId,
          senderId,
          content
        );
        const populatedMessage = {
          ...message._doc,
          senderId: { _id: senderId, name: senderName }
        };
        socket.emit("GET_MESSAGES", [populatedMessage]);
        socket.to(chatId).emit("GET_MESSAGES", [populatedMessage]);

        await ChatService.updateLatestMessage(chatId, message._id);
        socket.emit("RECENT_MESSAGE", { chatId, content });
        socket.broadcast.emit("RECENT_MESSAGE", { chatId, content });
      }
    );

    socket.on("UPDATE_READ", async (payload) => {
      const { chatId, userId, messageIds } = payload;
      await MessageService.updateReadStatusByIds(chatId, userId, messageIds);
      socket
        .to(chatId)
        .emit("READ_RECEIPT", { messageIds, dateTime: new Date(), userId });
    });

    socket.on("disconnect", async () => {
      console.log("Disconnected");
      await UserService.updateOnlineStatus(userId, false);
      socket.broadcast.emit("GET_OFFLINE_USERS", [userId]);
    });
  });
  // Error Handling
  userNamespace.on("connect_error", (error: Error) => {
    console.log("Connection error:", error.message);
  });
  userNamespace.on("error", (error: Error) => {
    console.log("Server-side error:", error.message);
  });
};
export default userSocket;
