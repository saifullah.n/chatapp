import joi from "joi";
import Validator from "../../middlewares/Validator.js";

const chatSchema = {
  chatName: joi.string().min(6),
  members: joi.array().min(2),
  isGroupChat: joi.boolean(),
  id: joi.string().min(3).max(255)
};

const Validators = {
  createChat: joi.object(
    Object.assign({}, chatSchema, {
      chatName: chatSchema.chatName.required(),
      members: chatSchema.members.required()
    })
  ),
  deleteChat: joi.object({ id: chatSchema.id.required() }),
  updateChat: joi.object({
    id: chatSchema.id.required(),
    userId: chatSchema.id.required()
  })
};

export default Validator(Validators);
