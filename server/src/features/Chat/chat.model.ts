import mongoose from "mongoose";
import { Schema } from "mongoose";

let chatSchema = new Schema(
  {
    chatName: {
      type: String
    },
    members: {
      type: [Schema.Types.ObjectId],
      ref: "User"
    },
    isGroupChat: {
      type: Boolean,
      default: false
    },
    lastMessage: {
      type: String,
      default: ""
    },
    latestMessage: {
      type: Schema.Types.ObjectId,
      ref: "Message"
    }
  },
  { timestamps: true }
);

export default mongoose.models.chat || mongoose.model("chat", chatSchema);
