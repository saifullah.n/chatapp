import { Request, Response } from "express";
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";
import { createChatService, createNewChat } from "./chat.service.js";
import { UserRequest } from "../User/user.types.js";

export const createChat = asyncErrorHandler(
  async (req: Request, res: Response) => {
    const userRequest = req as UserRequest;
    const { userId } = req.body;
    const chat = await createChatService(userId, userRequest.user_id);
    return res.status(201).send({
      chat
    });
  }
);

export const createGroupChat = asyncErrorHandler( async (req: Request, res: Response) => {
  const {chatName, members, isGroupChat} = req.body;
  const chat = await createNewChat(chatName, members, isGroupChat);
  return res.status(201).send({
      chat
  })
})
