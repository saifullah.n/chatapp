import Chat from "./chat.model.js";
import dbErrorHandler from "../../utils/dbErrorHandler.js";

export const createChat = dbErrorHandler(
  async (chatName: string, members: string[], isGroupChat: boolean) => {
    const chat = await Chat.create({
      chatName,
      members,
      isGroupChat: !!isGroupChat
    });
    return chat;
  }
);

export const findChats = dbErrorHandler(
  async (constraint = {}, filters = {}) => {
    const chats = await Chat.find({ ...constraint }, { ...filters }).populate(
      "members",
      "name"
    );
    return chats;
  }
);

export const updateChatById = dbErrorHandler(
  async (id: string, payload: any) => {
    const chat = await Chat.findByIdAndUpdate(id, payload);
    return chat;
  }
);

export const deleteChat = dbErrorHandler(async (constraint) => {
  const deletedChat = Chat.findOneAndDelete(constraint);
  return deletedChat;
});
