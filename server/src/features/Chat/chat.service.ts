import mongoose from "mongoose";
import {
  createChat,
  deleteChat,
  findChats,
  updateChatById
} from "./chat.db.js";
import UserService from "../User/user.service.js";

export const getChatsForUser = async (id: string) => {
  const chats = await findChats({
    members: {
      $in: new mongoose.Types.ObjectId(id)
    }
  });
  return chats;
};

export const getChatById = async (id: string) => {
  const chat = await findChats({ _id: new mongoose.Types.ObjectId(id) });
  return chat[0];
};

export const addMembersToChat = async (id: string, userIds: string[]) => {
  const chat = await updateChatById(id, {
    $push: { members: { $each: userIds } }
  });
};

export const removeMembersFromChat = async (id: string, userIds: string[]) => {
  const chat = await updateChatById(id, {
    $push: { members: { $each: userIds } }
  });
  return chat;
};

export const createNewChat = async (
  chatName: string,
  members: string[],
  isGroupChat: boolean
) => {
  const chat = await createChat(chatName, members, isGroupChat);
  return chat;
};

export const createChatService = async (member1: string, member2: string) => {
  const response = await findChats({
    members: { $all: [member1, member2] },
    isGroupChat: false
  });
  if (response.length) {
    return { chatMsg: response[0], isAlreadyexists: true };
  }
  const memberName1 = await UserService.getUserInfo(member1);
  const memberName2 = await UserService.getUserInfo(member2);
  const chatName = memberName1.name + "+" + memberName2.name;
  const isGroupChat = false;
  const members = [member1, member2];
  const chat = await createChat(chatName, members, isGroupChat);
  return { chatMsg: chat, isAlreadyexists: false };
};

export const deleteChatById = async (id: string) => {
  const deletedChat = deleteChat({ _id: new mongoose.Types.ObjectId(id) });
  return deletedChat;
};

export const updateLatestMessage = async (
  chatId: string,
  messageId: string
) => {
  const chat = await updateChatById(chatId, { latestMessage: messageId });
  return chat;
};
