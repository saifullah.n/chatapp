import { Router } from "express";
import { httpVerifyToken } from "../../middlewares/verifyToken.js";
import { createChat, createGroupChat } from "./chat.controller.js"
const router = Router();

router.post("/createChat", httpVerifyToken, createChat);
router.post("/createGroupChat", httpVerifyToken, createGroupChat);

export default router;
