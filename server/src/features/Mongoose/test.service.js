import { MongoMemoryServer } from "mongodb-memory-server";
import mongoose from "mongoose";

export const connect = async () => {
  const mongoServer = await MongoMemoryServer.create();
  await mongoose.connect(mongoServer.getUri());
};
export const disconnect = async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
};
