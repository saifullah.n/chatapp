import { Schema, model } from "mongoose";

let userReceipt = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: [true, "User ID required"],
    ref: "User"
  },
  readTime: {
    type: Date,
    required: [true, "Read dateTime required"]
  }
});

let messageSchema = new Schema(
  {
    chatId: {
      type: Schema.Types.ObjectId,
      required: [true, "ChatId required"]
    },
    senderId: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: [true, "Sender ID field required"]
    },
    content: {
      type: String,
      minLength: [1, "Empty message"]
    },
    readBy: {
      type: [userReceipt],
      default: []
    }
  },
  { timestamps: true }
);

export default model("Message", messageSchema);
