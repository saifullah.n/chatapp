import mongoose from "mongoose";
import { updateChatById } from "../Chat/chat.db.js";
import { updateMany, createNewMessage, findMessages } from "./message.db.js";

export const MessageService = {
  createNewMessage: async (
    chatId: string,
    senderId: string,
    content: string
  ) => {
    const response = await createNewMessage(chatId, senderId, content);
    await updateChatById(chatId, { lastMessage: content });
    return response.populate("senderId", "name");
  },
  findMessagesByChatId: async (chatId: string) => {
    const filterConditions = { chatId: new mongoose.Types.ObjectId(chatId) };
    const response = await findMessages(filterConditions, {updatedAt : 1, content: 1, createdAt: 1});
    return response;
  },
  updateReadStatusByIds: async (
    chatId: string,
    userId: string,
    msgIds: string[]
  ) => {
    const filterConditions = {
      chatId: new mongoose.Types.ObjectId(chatId),
      _id: { $in: msgIds }
    };
    const updateAction = {
      $push: {
        readBy: {
          userId: new mongoose.Types.ObjectId(userId),
          readTime: new Date()
        }
      }
    };
    const response = await updateMany(filterConditions, updateAction);
    return response;
  }
};
