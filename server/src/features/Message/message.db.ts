import Message from "./message.model.js";
import dbErrorHandler from "../../utils/dbErrorHandler.js";

export const createNewMessage = dbErrorHandler(
  async (chatId: string, senderId: string, content: string) => {
    const message = new Message({
      chatId: chatId,
      senderId: senderId,
      content: content
    });
    const response = await message.save();
    return response;
  }
);
export const findMessages = async (
  filter = {},
  projections = {},
  options = {}
) => {
  const response = await Message.find(
    { ...filter },
    { ...projections },
    { ...options }
  )
    .populate("senderId", "name")
    .populate("readBy")
    .exec();
  return response;
};
export const updateMany = async (filter = {}, update = {}) => {
  const response = await Message.updateMany({ ...filter }, { ...update });
  return response;
};
