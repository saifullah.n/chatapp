import { Request } from "express";
export type SearchUserQuery = {
  term: string;
};
export type UserRequest = Request & {
  name: string;
  user_id: string;
};