import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import {
  findUserByEmail,
  createNewUser,
  findUserById,
  findUsers,
  updateUser
} from "./user.db.js";
import CustomError from "../../utils/CustomError.js";
import mongoose from "mongoose";
import axios from "axios"
import {OAuth2Client} from "google-auth-library"

const client = new OAuth2Client();

const verify =  async(token: string) => {
  const ticket = await client.verifyIdToken({
      idToken: token,
      audience: process.env.CLIENT_ID,
  });
  const payload = ticket.getPayload();
  const email = payload ? payload['email'] : null;
  return email;
}

const UserService = {
  loginUser: async (email: string, password: string) => {
    const existingUser = await findUserByEmail(email);
    if (!existingUser) {
      throw new CustomError("User not found", 401);
    }
    const passwordMatch = await bcrypt.compare(password, existingUser.password);
    if (passwordMatch) {
      let data = {
        user_id: existingUser._id,
        name: existingUser.name
      };
      const token = jwt.sign(data, process.env.ACCESS_SECRET, {
        expiresIn: "2h"
      });
      return { accessToken: token, userData: data };
    } else {
      throw new CustomError("Wrong Credentials", 401);
    }
  },
  createUser: async (name: string, email: string, password: string) => {
    const hashedPassword = await bcrypt.hash(password, 10);
    return await createNewUser(name, email, hashedPassword);
  },
  loginUserViaGoogle: async(token: string) => {
    console.log({token});
    const email = await verify(token);
    const existingUser = await findUserByEmail(email);
    console.log({existingUser});
    if (!existingUser) {
      throw new CustomError("User not found", 401);
    }
    let data = {
      user_id: existingUser._id,
      name: existingUser.name
    };
    const accessToken = jwt.sign(data, process.env.ACCESS_SECRET, {
      expiresIn: "2h"
    });
    console.log({ accessToken , userData: data });
    return { accessToken , userData: data };
  },
  getUserInfo: async (userId: string) => {
    const user = await findUserById(userId);
    if (!user) {
      throw new CustomError("User not found", 400);
    }
    return {
      name: user.name,
      email: user.email,
      isOnline: user.isOnline
    };
  },
  getUsersList: async (userId: string) => {
    const users = await findUsers(
      { _id: { $ne: userId } },
      { _email: 0, password: 0 }
    );
    return users;
  },
  updateOnlineStatus: async (userId: string, status: boolean) => {
    const user = await updateUser(userId, { isOnline: status });
    return user;
  },
  getOnlineUser: async (userId: string) => {
    const onlineUsers = await findUsers(
      { _id: { $ne: new mongoose.Types.ObjectId(userId) }, isOnline: true },
      { _id: 1 }
    );
    return onlineUsers;
  },
  getUsersListByTerm: async (term: string) => {
    const usersList = await findUsers({
      $or: [
        {
          name: {
            $regex: `^${term}`,
            $options: "i"
          }
        },
        {
          email: {
            $regex: `^${term}`,
            $options: "i"
          }
        }
      ]
    });
    return usersList;
  }
};

export default UserService;
