import User from "./user.model.js";
import dbErrorHandler from "../../utils/dbErrorHandler.js";

export const createNewUser = dbErrorHandler(
  async (name: string, email: string, password: string) => {
    const user = new User({
      name: name,
      email: email,
      isOnline: false,
      password: password
    });
    const response = await user.save();
    return response;
  }
);

export const findUserByEmail = dbErrorHandler(async (email: string) => {
  const user = await User.findOne({ email: email }).lean();
  return user;
});

export const findUserById = dbErrorHandler(async (id: string) => {
  const user = await User.findById(id).lean();
  return user;
});

export const findUsers = dbErrorHandler(
  async (constraint = {}, filters = {}) => {
    const userData = await User.find({ ...constraint }, { ...filters });
    return userData;
  }
);

export const updateUser = dbErrorHandler(async (id: string, payload: any) => {
  const userData = await User.findByIdAndUpdate(id, payload);
  return userData;
});
