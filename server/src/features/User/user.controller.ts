import { Request, Response } from "express";
import UserService from "./user.service.js";
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";
import { SearchUserQuery, UserRequest } from "./user.types.js";

// Register User
export const createUser = asyncErrorHandler(
  async (req: Request, res: Response) => {
    const { name, email, password } = req.body;
    await UserService.createUser(name, email, password);
    return res.status(201).send("User created successfully");
  }
);

// Login user
export const loginUser = asyncErrorHandler(
  async (req: Request, res: Response) => {
    if(req.body?.GoogleAccessToken){
      const {GoogleAccessToken} = req.body;
      const googletoken = await UserService.loginUserViaGoogle(GoogleAccessToken);
      res.status(200).send(googletoken);
    }
    else{
      const { email, password } = req.body;
      const accessToken = await UserService.loginUser(email, password);
      res.status(200).send(accessToken);
    }
  }
);

// Get currently logged in user details
export const getUserInfo = asyncErrorHandler(
  async (req: Request, res: Response) => {
    const userReq = req as UserRequest;
    const userInfo = { name: `${userReq.name}`, user_id: `${userReq.user_id}` };
    //const userInfo = await UserService.getUserInfo(userId);
    return res.status(200).send(userInfo);
  }
);

//Get users based on term
export const searchForUser = asyncErrorHandler(
  async (req: Request, res: Response) => {
    const searchUserQuery: SearchUserQuery = {
      term: `${req.query.term}`
    };
    const usersList = await UserService.getUsersListByTerm(
      searchUserQuery.term
    );
    return res.status(200).send({
      usersList
    });
  }
);
