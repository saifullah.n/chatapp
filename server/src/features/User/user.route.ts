import { Router } from "express";
import {
  createUser,
  loginUser,
  getUserInfo,
  searchForUser
} from "./user.controller.js";
import { httpVerifyToken } from "../../middlewares/verifyToken.js";
import Validator from "./user.validator.js";

const router = Router();

router.post("/register", Validator("register"), createUser);
router.post("/login", Validator("login"), loginUser);
router.get("/get-info", httpVerifyToken, getUserInfo);
router.get("/searchForUser", searchForUser);

export default router;
