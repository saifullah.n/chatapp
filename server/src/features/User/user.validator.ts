import joi from "joi";
import Validator from "../../middlewares/Validator.js";

const invalidPasswordMessages = {
  "string.empty": "Password is empty",
  "string.min": "Password must have minimum 8 characters",
  "any.required": "Password required"
};
const invalidEmailMessages = {
  "string.empty": "Email is empty",
  "string.email": "Invalid email",
  "any.required": "Email required"
};

const loginSchema = joi.object({
  email: joi
    .string()
    .email()
    .lowercase()
    .messages({ ...invalidEmailMessages }),
  password: joi
    .string()
    .min(8)
    .messages({ ...invalidPasswordMessages }),
  GoogleAccessToken : joi
    .string()
});
const registerSchema = joi.object({
  name: joi.string().min(4).required().messages({
    "string.min": "Username must have minimum 4 characters",
    "any.required": "Username required"
  }),
  email: joi
    .string()
    .email()
    .lowercase()
    .required()
    .messages({ ...invalidEmailMessages }),
  password: joi
    .string()
    .min(8)
    .required()
    .messages({ ...invalidPasswordMessages })
});

const Validators = {
  login: loginSchema,
  register: registerSchema
};

export default Validator(Validators);
